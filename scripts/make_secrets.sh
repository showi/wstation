#!/bin/bash

INFLUXDB_SECRET=secrets/influxdb.env

if [[ -e $INFLUXDB_SECRET ]]; then
    echo "InfluxDB secret exists in ${INFLUXDB_SECRET}"
else
    cp scripts/influxdb.secret.template ${INFLUXDB_SECRET}
    echo "InfluxDB secret template copied in ${INFLUXDB_SECRET}"
fi

