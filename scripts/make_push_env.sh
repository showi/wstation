#!/usr/bin/bash

envs=""
for e in $(cat ./secrets/influxdb.env); do envs="${envs} --env ${e}"; done
echo ${envs}