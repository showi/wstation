#!/usr/bin/env python3

import sys
import os

module_path = os.path.abspath(os.path.join(os.path.dirname(__file__)))

sys.path.insert(0, module_path)

if __name__ == "__main__":
    import logging
    import json
    from collector.collect import collect
    from collector.logger import getLogger

    logging.basicConfig()
    logger = getLogger(__file__)
    from collector.config import base_config
    from collector.utils import make_config
    from collector.sensors.plugin import load_plugin

    config = make_config(base_config)
    sensors = []
    for name in config["sensor_plugins"]:
        try:
            plugin = load_plugin(name)
            sensor = plugin.PluginSensor(config)
            sensor.setup()
            sensors.append(sensor)
            logger.debug("Registered sensor: {}".format(name))
        except Exception as e:
            logger.error("PluginImportError: {}".format(e))
    logger.debug("Collect sensors data every {}s".format(config["collect_interval"]))
    try:
        collect(config, sensors)
    except KeyboardInterrupt:
        pass