import os
import datetime
import json
from collector.config import base_path


def get_env(name, default_value=None):
    value = os.environ.get(name)
    if value is None:
        return default_value
    return value


def make_timestamp():
    return datetime.datetime.now().timestamp()


def write(write_path, data_str):
    with open(write_path, "w") as wh:
        wh.write(data_str)


def make_data(**ka):
    if "timestamp" not in ka:
        ka["timestamp"] = make_timestamp()
    return json.dumps(ka, indent=None)


def make_config(config):
    if not os.path.exists(config["data_path"]):
        config["data_path"] = os.path.join(base_path, "data")
    if not os.path.exists(config["data_path"]):
        os.mkdir(config["data_path"])
    config.update(
        {
            "location": get_env("WS_LOCATION", "paris"),
            "room": get_env("WS_ROOM", "chambre"),
        }
    )
    return config
