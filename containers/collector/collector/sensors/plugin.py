import os
from collector.config import base_path
from collector.logger import getLogger

logger = getLogger(__file__)


import importlib.util
import sys


def import_module(module_name, file_path):
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    spec.loader.exec_module(module)
    return module


def load_plugin(name):
    module_name = "{}_sensor".format(name)
    file_path = os.path.join(
        base_path, "collector", "sensors", "plugins", name, "sensor.py"
    )
    module = import_module(module_name, file_path)
    return module