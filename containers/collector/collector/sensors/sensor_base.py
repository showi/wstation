import os
from collector.utils import make_data, write


def make_sensor_write_path(sensor):
    return os.path.join(
        sensor.config["data_path"],
        "sensor_{location}_{room}_{sensor_name}_{sensor_id}.json".format(
            location=sensor.config["location"],
            room=sensor.config["room"],
            sensor_name=sensor.sensor_name,
            sensor_id=sensor.sensor_id,
        ),
    )


class Sensor:
    def __init__(self, config, **ka):
        self.config = config
        if not "sensor_id" in ka:
            ka["sensor_id"] = 0
        [setattr(self, k, ka[k]) for k in ka]
        self.setup()

    def make_common_data(self, **ka):
        g = lambda k: self.config[k]
        data = make_data(
            name=self.name,
            location=g("location"),
            room=g("room"),
            sensor_name=self.sensor_name,
            sensor_id=self.sensor_id,
            **ka
        )
        return data

    def setup(self):
        raise NotImplementedError()

    def collect(self):
        raise NotImplementedError()