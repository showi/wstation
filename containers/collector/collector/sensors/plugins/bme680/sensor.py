import bme680
from collector.logger import getLogger
from collector.sensors.sensor_base import make_sensor_write_path, Sensor
from collector.utils import make_timestamp, write, make_data

logger = getLogger(__file__)


class PluginSensor(Sensor):
    def __init__(self, config, **ka):
        ka.update({"name": "climate", "sensor_name": "bme680"})
        super(PluginSensor, self).__init__(config, **ka)

    def _make_sensor(self):
        sensor = None
        try:
            sensor = bme680.BME680(bme680.I2C_ADDR_PRIMARY)
        except IOError:
            sensor = bme680.BME680(bme680.I2C_ADDR_SECONDARY)
        self.sensor = sensor
        return sensor

    def setup(self):
        sensor = self._make_sensor()
        sensor.set_humidity_oversample(bme680.OS_2X)
        sensor.set_pressure_oversample(bme680.OS_4X)
        sensor.set_temperature_oversample(bme680.OS_8X)
        sensor.set_filter(bme680.FILTER_SIZE_3)
        sensor.set_gas_status(bme680.ENABLE_GAS_MEAS)
        sensor.set_gas_heater_temperature(320)
        sensor.set_gas_heater_duration(150)
        sensor.select_gas_heater_profile(0)

    def collect(self):
        sensor = self.sensor
        if not sensor.get_sensor_data():
            return False
        write(
            make_sensor_write_path(self),
            self.make_common_data(
                temperature=sensor.data.temperature,
                pressure=sensor.data.pressure,
                humidity=sensor.data.humidity,
                gas_resistance=sensor.data.gas_resistance,
            ),
        )
