from as7262 import AS7262
from collector.logger import getLogger
from collector.sensors.sensor_base import make_sensor_write_path, Sensor
from collector.utils import make_timestamp, write, make_data

logger = getLogger(__file__)


class PluginSensor(Sensor):
    def __init__(self, config, **ka):
        ka.update({"name": "spectral", "sensor_name": "as7262"})
        super(PluginSensor, self).__init__(config, **ka)

    def _make_sensor(self):
        self.sensor = AS7262()
        return self.sensor

    def setup(self):
        sensor = self._make_sensor()
        sensor.set_gain(64)
        sensor.set_integration_time(17.857)
        sensor.set_measurement_mode(2)
        sensor.set_illumination_led(0)

    def collect(self):
        sensor = self.sensor
        [red, orange, yellow, green, blue, violet] = sensor.get_calibrated_values()
        return write(
            make_sensor_write_path(self),
            self.make_common_data(
                red=red,
                orange=orange,
                yellow=yellow,
                green=green,
                blue=blue,
                violet=violet,
            ),
        )
