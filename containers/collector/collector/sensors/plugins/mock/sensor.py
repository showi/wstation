from collector.logger import getLogger
from collector.sensors.sensor_base import make_sensor_write_path, Sensor
from collector.utils import make_timestamp, write, make_data

logger = getLogger(__file__)


class PluginSensor(Sensor):
    def __init__(self, config, **ka):
        ka.update(
            {
                "name": "mock",
                "sensor_name": "mock-sensor",
                "collect_id": 0,
            }
        )
        super(PluginSensor, self).__init__(config, **ka)

    def setup(self):
        pass

    def collect(self):
        write(
            make_sensor_write_path(self),
            self.make_common_data(collect_id=self.collect_id),
        )
        self.collect_id += 1
