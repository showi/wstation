import json
import os
import time

from collector.logger import getLogger
from collector.utils import get_env

logger = getLogger(__file__)


def collect(config, sensors):
    while True:
        [sensor.collect() for sensor in sensors]
        time.sleep(config["collect_interval"])
