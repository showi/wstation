import os

base_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))

base_config = {
    "data_path": "/mnt/data-volume/",
    "collect_interval": 10,
    "sensor_plugins": ["bme680", "as7262"],
}
