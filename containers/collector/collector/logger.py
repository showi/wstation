import logging
import os
from collector.config import base_path


def getLogger(file_path):
    name = file_path[len(base_path) :]
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    return logger
