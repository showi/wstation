#!/usr/bin/env python3

import sys
import os

module_path = os.path.abspath(os.path.join(os.path.dirname(__file__)))

sys.path.insert(0, module_path)

if __name__ == "__main__":
    import logging
    import json

    logging.basicConfig()
    from display.config import base_config
    from display.utils import make_config
    from display.oled import update

    config = make_config(base_config)
    try:
        update(config)
    except KeyboardInterrupt:
        pass