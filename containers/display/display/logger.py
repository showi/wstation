import logging
import os
from display.config import base_path


def getLogger(file_path):
    name = file_path[len(base_path) :]
    logger = logging.getLogger(name)
    logger.setLevel(logging.ERROR)
    return logger
