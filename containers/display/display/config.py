import os

base_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))

base_config = {
    "data_path": "/mnt/data-volume/",
    "update_interval": 1,
    "sensor_file": "sensor_paris_chambre_bme680_0",
}
