import os
import time
import requests
import geocoder
import lxml
import datetime
from bs4 import BeautifulSoup
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from luma.core.interface.serial import i2c
from luma.core.error import DeviceNotFoundError
from luma.oled.device import sh1106
from display.logger import getLogger
from display.utils import read_sensor_data, get_font, get_image_path

logger = getLogger(__file__)


def render(oled, config, initial_data):
    sensor_data = read_sensor_data(config)
    gid = lambda k: initial_data[k]
    gsd = lambda k: sensor_data[k]
    temp = gsd("temperature")
    press = gsd("pressure")
    background = Image.open(get_image_path("weather.png")).convert(oled.mode)
    draw = ImageDraw.ImageDraw(background)
    if datetime.datetime.today().day == gid("curr_date"):
        if temp < gid("low_temp"):
            initial_data["low_temp"] = temp
        elif temp > gid("high_temp"):
            initial_data["high_temp"] = temp
    else:
        initial_data["curr_date"] = datetime.datetime.today().day
        initial_data["low_temp"] = temp
        initial_data["high_temp"] = temp

    # Write temp. and press. to image
    draw.text((8, 22), "{0:4.0f}".format(press), fill="white", font=gid("rb_20"))
    draw.text((86, 12), u"{0:2.0f}°".format(temp), fill="white", font=gid("rb_20"))

    # Write min and max temp. to image
    draw.text(
        (80, 0),
        u"max: {0:2.0f}°".format(gid("high_temp")),
        fill="white",
        font=gid("rr_12"),
    )
    draw.text(
        (80, 110),
        u"min: {0:2.0f}°".format(gid("low_temp")),
        fill="white",
        font=gid("rr_12"),
    )

    # Write the 24h time and blink the separator every second
    if int(time.time()) % 2 == 0:
        draw.text(
            (4, 98),
            datetime.datetime.now().strftime("%H:%M"),
            fill="white",
            font=gid("rr_24"),
        )
    else:
        draw.text(
            (4, 98),
            datetime.datetime.now().strftime("%H %M"),
            fill="white",
            font=gid("rr_24"),
        )

    # These lines display the temp. on the thermometer image
    draw.rectangle([(97, 43), (100, 86)], fill="black")
    temp_offset = 86 - ((86 - 43) * ((temp - 20) / (32 - 20)))
    draw.rectangle([(97, temp_offset), (100, 86)], fill="white")

    # Display the completed image on the OLED
    oled.display(background)


def update(config):
    oled = sh1106(i2c(), rotate=2, height=128, width=128)
    sensor_data = read_sensor_data(config)
    rr_path = get_font("Roboto-Regular.ttf")
    rb_path = get_font("Roboto-Black.ttf")
    initial_data = {
        "low_temp": sensor_data["temperature"],
        "high_temp": sensor_data["temperature"],
        "curr_date": datetime.date.today().day,
        "rr_24": ImageFont.truetype(rr_path, 24),
        "rb_20": ImageFont.truetype(rb_path, 20),
        "rr_12": ImageFont.truetype(rr_path, 12),
    }
    while True:
        render(oled, config, initial_data)
        time.sleep(config["update_interval"])
