import os
import datetime
import json
from display.config import base_path


def make_config(config):
    return config


def read_json(json_path):
    with open(json_path, "r", encoding="utf8") as rh:
        return json.load(rh)


def read_sensor_data(config):
    sensor_path = os.path.join(
        config["data_path"], "{}.json".format(config["sensor_file"])
    )
    return read_json(sensor_path)


def get_font(name):
    return os.path.abspath(os.path.join(os.path.dirname(__file__), "fonts", name))


def get_image_path(name):
    return os.path.join(base_path, "display", "images", name)