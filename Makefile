APPLICATION=wstation
DEVICE_NAME=519eb48
DEVICE_LOCAL_HOST=$(DEVICE_NAME).local
DEVICE_IMG=img/balena-cloud-raspberry-pi-2.54.2+rev1-dev-v11.12.4.img

.PHONY: ping
ping:
	ping $(DEVICE_NAME).local

.PHONY: configure
configure:
	sudo balena local configure $(DEVICE_IMG)

.PHONY: connect
connect:
	balena ssh $(DEVICE_LOCAL_HOST) $(service)

.PHONY: push
push:
	echo $(ENVS)
	cd containers; balena push $(DEVICE_LOCAL_HOST) $(options) $(shell bash scripts/make_push_env.sh)

.PHONY: pushr
pushr:
	cd containers; balena push $(APPLICATION) $(options)

.PHONY: info
info:
	@echo Name: $(DEVICE_NAME)
	@echo Host: $(DEVICE_LOCAL_HOST)

.PHONY: build-py-sensors
build-py-sensors:
	cd containers/collector; cat Dockerfile.template| sed s/%%BALENA_MACHINE_NAME%%/rpi/ | docker build -f - . --target BUILD --tag showi/balena-rpi-debian:stretch-run-python-sensors \
	&& docker push showi/balena-rpi-debian:stretch-run-python-sensors


.PHONY: clean
clean:
	find containers  -iname "*.pyc" -exec rm {} \;
	rm -rf containers/collector/data

.PHONY: collect
collect:
	python ./containers/collector/run.py
