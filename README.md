# wstation

Sensors report from Raspberry PI to InfluxDB using BalenaCloud.

* note: sensor code adapted from [Pimoroni Breakout Garden](https://github.com/pimoroni/breakout-garden) github repository.

```mermaid
graph LR;
subgraph Containers;
C(Collector) -->|write|D[(data)];
D-->|read|Di(Display);
D-->|read|T(Telegraf);
style C fill:#09f,stroke:#333,stroke-width:4px
style Di fill:#09f,stroke:#333,stroke-width:4px
style T fill:#09f,stroke:#333,stroke-width:4px
end
subgraph InfluxDB Cloud;
T-->|write|I(InfluxDb)
style I fill:#f90,stroke:#333,stroke-width:4px
end
```

## Prerequisite

### Devices & Materials

* Raspberry Pi Zero WH
* Breakout Garden for Raspberry Pi (I2C)
* BME680 Breakout - Air Quality, Temperature, Pressure, Humidity Sensor (I2C)
* AS7262 6-channel Spectral Sensor (Spectrometer) Breakout (I2C)
* 1.12" Mono OLED (128x128, white/black) Breakout (I2C)

### SaaS

Account needed on these services

* https://www.influxdata.com/products/influxdb-cloud/ (Free plan / 30 days retention)
* https://www.balena.io/cloud/ (Free plan / 10 devices)

## Install

## InfluxDb Cloud

* create `Telegraf` token.

## Balena Cloud

* create application named `wstation`.
* create device:
  * configure WIFI
  * download image
  * burn image

### Environment variables

Create environment variables with InfluxDB `Telegraf` token.

See `./scripts/influxdb.secret.template`

For local development you need to push with these variables.
You can create `secrets/influxdb.env` with the script `scripts/make_secrets.sh`.
See target `push`in `Makefile` for example.

### Push code (cloud)

If you have burn the PI image you just need to push containers.

```sh
cd containers
balena push wstation

```

### Push code (local)

You should enable local mode in Balena Cloud interface.

```sh
    cd containers
    sudo balena scan
    balena push [uuid|local device ip]
```

## Links

* [telegraf - configuration](https://github.com/influxdata/telegraf/blob/master/docs/CONFIGURATION.md)
* [Run Docker containers on embedded devices](https://www.balena.io)
* [Telegraf / InfluxDB / Grafana on RaspberryPi – From Scratch](https://nwmichl.net/2020/07/14/telegraf-influxdb-grafana-on-raspberrypi-from-scratch/)
* [Creating a BLE Peripheral with BlueZ](https://punchthrough.com/creating-a-ble-peripheral-with-bluez/)
* [Building Minimal Docker Containers for Python Applications](vhttps://blog.realkinetic.com/building-minimal-docker-containers-for-python-applications-37d0272c52f3)
